package src.builders;

import src.models.Magazine;
import src.models.Product;
import src.models.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MagazineBuilder {	
	
	private static final String PHONE_REGEX = "^+380[1-9]{9}$";
    private static final int MIN_CATEGORIES_NUMBER = 1;
    private static final int MAX_CATEGORIES_NUMBER = 50;
    private static final int MIN_ADDRESS_SIZE = 10;
    private static final int MAX_ADDRESS_SIZE = 70;

    private String address = "";
    private String phone = "";
	public List<Category> categories = new ArrayList<>();

    public MagazineBuilder() { }

    public MagazineBuilder(String address) {
        this.address = address;
    }

    public MagazineBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    public MagazineBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public MagazineBuilder setCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Magazine build() {
        StringBuilder errorMessage = new StringBuilder("");

        Magazine magazine = new Magazine();
        Pattern phonePattern = Pattern.compile(PHONE_REGEX);

        Matcher phoneMatcher = phonePattern.matcher(phone);

        if (!phoneMatcher.matches())
            errorMessage.append("Enter correct magazine phone");
        if (categories.size() < MIN_CATEGORIES_NUMBER || categories.size() > MAX_CATEGORIES_NUMBER) {
            errorMessage.append("Enter correct categories list");
        }

        if (address.length() < MIN_ADDRESS_SIZE || address.length() > MAX_ADDRESS_SIZE) {
            errorMessage.append("Enter correct address");
        }

        magazine.setAdderess(address);
        magazine.setPhone(phone);
        magazine.setCategories(categories);

        return magazine;
    }
}
