package src.database;

import src.builders.CategoryBuilder;
import src.builders.MagazineBuilder;
import src.builders.ProductBuilder;
import src.models.Magazine;
import src.models.Category;
import src.models.Product;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Database {

    private static final String URL = "jdbc:mysql://localhost:3306/java";
    private static final String USER = "root";
    private static final String PASSWORD = "";
    
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
   
    private Database() {
        try {
        	con = DriverManager.getConnection(URL, USER, PASSWORD);
        	stmt = con.createStatement();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
    
    protected void finalize() {
        try { 
        	con.close(); 
        } 
        catch(SQLException e) {
        	e.printStackTrace();
        }
        try { 
        	stmt.close(); 
        } 
        catch(SQLException e) {
        	e.printStackTrace();
        }
        try { 
        	rs.close(); 
        } catch(SQLException e) {
        	e.printStackTrace();
        }
    }

    public void createProductTable() {
    	String query = "CREATE TABLE IF NOT EXISTS product (" +
    		"id SERIAL NOT NULL PRIMARY KEY," +
    		"title text NOT NULL," +
    		"category_id int(2) NOT NULL," +
    		"description text NOT NULL," +
    		"price float NOT NULL," +
    		"count int(11) NOT NULL" +
    		") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    	try {
    		stmt.executeUpdate(query);
         
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void createCategoryTable() {
    	String query = "CREATE TABLE IF NOT EXISTS category (" +
    		"id SERIAL NOT NULL PRIMARY KEY," +
    		"name text NOT NULL," +
    		"parent_id int(11) NOT NULL" +
    		") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    	try {
    		stmt.executeUpdate(query);
         
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void createMagazineTable() {
    	String query = "CREATE TABLE IF NOT EXISTS magazine (" +
    		"id SERIAL NOT NULL PRIMARY KEY," +
    		"address text NOT NULL," +
    		"phone text NOT NULL" +
    		") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    	try {
    		stmt.executeUpdate(query);
         
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
    }
    
    public void addProduct(Product product) {
    	String query = "INSERT INTO product (title, category_id, description, price, count) VALUES (?, ?, ?, ?, ?);";
    	try  {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setString(1, product.getTitle());
    		statement.setInt(2, product.getCategory_id());
    		statement.setString(3, product.getDescription());
    		statement.setDouble(4, product.getPrice());
    		statement.setInt(5, product.getCount());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void addProduct(Product product, int id) {
    	String query = "INSERT INTO product (id, title, category_id, description, price, count) VALUES (?, ?, ?, ?, ?, ?);";
    	try  {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setInt(1, id);
    		statement.setString(2, product.getTitle());
    		statement.setInt(3, product.getCategory_id());
    		statement.setString(4, product.getDescription());
    		statement.setDouble(5, product.getPrice());
    		statement.setInt(6, product.getCount());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void addCategory(Category category) {
    	String query = "INSERT INTO category (name, parent_id) VALUES (?, ?);";
    	try  {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setString(1, category.getName());
    		statement.setInt(2, category.getParentId());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void addCategory(Category category, int id) {
    	String query = "INSERT INTO category (id, name, parent_id) VALUES (?, ?, ?);";
    	try  {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setInt(1, id);
    		statement.setString(2, category.getName());
    		statement.setInt(3, category.getParentId());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void addMagazine(Magazine magazine) {
    	String query = "INSERT INTO magazine (address, phone) VALUES (?, ?);";
    	try  {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setString(1, magazine.getAddress());
    		statement.setString(2, magazine.getPhone());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void addMagazine(Magazine magazine, int id) {
    	String query = "INSERT INTO magazine (id, address, phone) VALUES (?, ?, ?);";
    	try  {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setInt(1, id);
    		statement.setString(2, magazine.getAddress());
    		statement.setString(3, magazine.getPhone());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void deleteProduct(int id) {
    	String query = "DELETE FROM product WHERE id =" + id;
    	try {
    		stmt.executeUpdate(query);
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void deleteCategory(int id) {
    	String query = "DELETE FROM category WHERE id =" + id;
    	try {
    		stmt.executeUpdate(query);
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void deleteMagazine(int id) {
    	String query = "DELETE FROM magazine WHERE id=" + id;
    	try {
    		stmt.executeUpdate(query);
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void updateProduct(Product product) {
    	String query = "UPDATE product SET title=?, category_id = ?, description=?, price=?, count=? WHERE id=" + product.getId();
    	try {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setString(1, product.getTitle());
    		statement.setInt(2, product.getCategory_id());
    		statement.setString(3, product.getDescription());
    		statement.setDouble(4, product.getPrice());
    		statement.setInt(5, product.getCount());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void updateCategory(Category category) {
    	String query = "UPDATE category SET name=?, parent_id = ? WHERE id=" + category.getId();
    	try {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setString(1, category.getName());
    		statement.setInt(2, category.getParentId());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void updateMagazine(Magazine magazine) {
    	String query = "UPDATE magazine SET address=?, phone=? WHERE id=" + magazine.getId();
    	try {
    		PreparedStatement statement = con.prepareStatement(query);
    		statement.setString(1, magazine.getAddress());
    		statement.setString(2, magazine.getPhone());
    		statement.executeUpdate();
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public Product getProductById(int id) {
    	String query = "SELECT * FROM product WHERE id=" + id;
    	Product product = new Product();
    	try {
    		rs = stmt.executeQuery(query);
    		rs.next();
    		product.setId(rs.getInt(1));
    		product.setTitle(rs.getString(2));
    		product.setCategory_id(rs.getInt(3));
    		product.setDescription(rs.getString(4));
    		product.setPrice(rs.getDouble(5));
    		product.setCount(rs.getInt(6));
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    	return product;
    }
    
    public List<Product> getProductsByCategoryId(int id) {
    	String query = "SELECT * FROM product WHERE category_id=" + id;
		List<Product> list = new ArrayList<>();
    	try {
    		rs = stmt.executeQuery(query);
    		while(rs.next()) {
    			ProductBuilder builder = new ProductBuilder();
    			Product product = builder
    					.setId(rs.getInt(1))
    					.setTitle(rs.getString(2))
    					.setCategory_id(rs.getInt(3))
    					.setDescription(rs.getString(4))
    					.setPrice(rs.getDouble(5))
    					.setCount(rs.getInt(6))
    					.build();
    			
    			list.add(product);
    		}
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    	return list;
    }
    
    public Category getCategoryById(int id) {
    	String query = "SELECT * FROM category WHERE id=" + id;
    	Category category = new Category();
    	try {
    		rs = stmt.executeQuery(query);
    		rs.next();
    		category.setId(rs.getInt(1));
    		category.setName(rs.getString(2));
    		category.setParentId(rs.getInt(3));
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    	return category;
    }
    
    public Magazine getMagazineById(int id) {
    	String query = "SELECT * FROM magazine WHERE id=" + id;
    	Magazine magazine = new Magazine();
    	try {
    		rs = stmt.executeQuery(query);
    		rs.next();
    		magazine.setId(rs.getInt(1));
    		magazine.setAdderess(rs.getString(2));
    		magazine.setPhone(rs.getString(3));
    	}
    	catch (SQLException e) {
            e.printStackTrace();
        } 
    	return magazine;
    }
    
    public void getEntityCount(String entity) {
    	String query = "select count(*) from " + entity;
    	try {
    		rs = stmt.executeQuery(query);
            while (rs.next()) {
                int count = rs.getInt(1);
                System.out.println("Total number of books in the table : " + count);
            }
    	}
    	catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }
    
    public static void main(String args[]) {
    	Database db = new Database();
    }
}