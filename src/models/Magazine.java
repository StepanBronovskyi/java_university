package src.models;

import java.util.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


@XmlRootElement
@XmlType(propOrder = {"address", "phone"})

public class Magazine implements Serializable{

	public int id;
	public String address;
	public String phone;
	public List<Category> categories;
	
	public Magazine() {}
	
	public Magazine(int id, String address, String phone) {
		this.id = id;
		this.address = address;
		this.phone = phone;
		this.categories = new ArrayList<Category>();
	}
	
	public void addCategory(Category c) {
		this.categories.add(c);
	}
	
	public void setCategories(List<Category> categories) {
	    this.categories = categories;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setPhone(String p) {
		this.phone = p;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	public void setAdderess(String a) {
		this.address = a;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + ", addess: " + this.address + ", phone: " + this.phone + ";";
	}
	
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        String phoneText = this.phone;
        result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
        result = prime * result + ((phoneText == null) ? 0 : phoneText.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Magazine toCompare = (Magazine) obj;
        if (!this.getAddress().equals(toCompare.getAddress()) ||
                this.getPhone().equals(toCompare.getPhone()) ||
                this.id != toCompare.getId())
            return false;
        return true;
    }
}
