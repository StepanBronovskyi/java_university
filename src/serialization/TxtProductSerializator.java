package src.serialization;

import src.builders.CategoryBuilder;
import src.builders.MagazineBuilder;
import src.builders.ProductBuilder;

import src.models.Category;
import src.models.Product;
import src.models.Magazine;

import java.io.*;
import java.time.LocalDate;
import java.util.Scanner;

public class TxtProductSerializator implements Serialization<Product> {

    @Override
    public void toFile(Product prod, String path) throws Exception {
        FileWriter outFile = null;

        try {
            outFile = new FileWriter(path);
            outFile.write(prod.toString());
        }
        finally {
            if(outFile != null)
                outFile.close();
        }
    }

    @Override
    public Product fromFile(String path) throws Exception {
    	Product prod = null;
        ProductBuilder productBuilder = null;
        FileReader inFile = null;
        Scanner input = null;
        String buff = null;
        String buffPrev = null;

        try {
            inFile = new FileReader(path);
            input = new Scanner(inFile);
            productBuilder = new ProductBuilder();
            input.useDelimiter("\n|(; ?)|:|( ?= ?)|, ");
            while(input.hasNext()) {
                buff = input.next().trim();
                switch(buff) {
	                case "id":
	                	productBuilder.setId(Integer.parseInt(input.next().trim()));
	                    break;
                    case "title":
                    	productBuilder.setTitle(input.next().trim());
                        break;
                    case "category_id":
	                	productBuilder.setCategory_id(Integer.parseInt(input.next().trim()));
	                    break;
                    case "description":
                    	productBuilder.setDescription(input.next().trim());
                        break;
                    case "count":
                    	productBuilder.setCount(Integer.parseInt(input.next().trim()));
                        break;
                    case "price":
                    	productBuilder.setPrice(Double.parseDouble(input.next().trim()));
                        break;
                }
            }
        }
        finally {
            if(inFile != null)
                inFile.close();
            if(input != null)
                input.close();
        }

        return productBuilder.build() ;
    }

    public static void main(String[] args) throws Exception {
        Product product = new Product(1, "Sonyxz1", 2, "mobilephone1", 1, 400);
        TxtProductSerializator serializator = new TxtProductSerializator();
        serializator.toFile(product,"file.txt");
        System.out.println(serializator.fromFile("file.txt"));
    }
}
