package src.models;
import java.util.*;
import java.lang.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;

@XmlRootElement
@XmlType(propOrder = {"title", "description", "count", "price"})

public class Product implements Serializable{

	public int id;
	public String title;
	public int category_id;
	public String description;
	public int count;
	public double price;
	
	public Product() {}
	
	public Product(int id, String title, int category_id, String description, int count, double price) {
		this.id = id;
		this.title = title;
		this.category_id = category_id;
		this.description = description;
		this.count = count;
		this.price = price;		
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setCategory_id(int id) {
		this.category_id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setCount(int count) {
		this.count = count;
	}	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getId() {
		return this.id;
	}
	public String getTitle() {
		return this.title;
	}
	public int getCategory_id() {
		return this.category_id;
	}
	public String getDescription() {
		return this.description;
	}
	public int getCount() {
		return this.count;
	}	
	public double getPrice() {
		return this.price;
	}
	
	@Override
    public int hashCode() {
        int result;
        long temp;
        result = this.title.hashCode();
        result = 31 * result + (this.description != null ? this.description.hashCode() : 0);
        temp = Integer.toUnsignedLong(this.count);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Integer.toUnsignedLong(this.category_id);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(this.price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
	
	@Override
	public String toString() {
		return "Title: " + this.title + 
				"Category: " + this.category_id +
				"Description: " + this.description +
				"Count: " + this.count + 
				"Price : " + this.price;
	}
	
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Product toCompare = (Product) obj;
        if (!this.getTitle().equals(toCompare.getTitle()) ||
        		!this.getDescription().equals(toCompare.getDescription()) ||
                this.getPrice() != toCompare.getPrice() || this.getCount() != toCompare.getCount() ||
                this.getCategory_id() != toCompare.getCategory_id())
            return false;
        return true;
    }
}
