package src.models;

import src.models.*;
import java.util.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


@XmlRootElement
@XmlType(propOrder = {"id", "name", "parent_id"})

public class Category implements Serializable {
	
	public int id;
	public String name;
	public int parent_id;
	public List<Product> products;
	
	public Category() {}
	
	public Category(int id, String name, int parent_id) {
		this.id = id;
		this.name = name;
		this.parent_id = parent_id;
		this.products = new ArrayList<Product>();
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setParentId(int id) {
		this.parent_id = id;
	}
	
	public int getParentId() {
		return this.parent_id;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public void addProduct(Product product) {
		this.products.add(product);
	}

	@Override
	public String toString() {
		return "Category: " + this.name + "Id: " + this.id +
				"Parent Id: " + this.parent_id;
	}
	
	@Override
    public int hashCode() {
        final int prime = 31;
        int result = name.hashCode();
        long temp1 = Integer.toUnsignedLong(this.id);
        long temp2 = Integer.toUnsignedLong(this.parent_id);
        result = prime * result + (int)temp1;
        result = prime * result + (int)temp2;
        return result;
    }
	
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Category toCompare = (Category) obj;
        if (!this.getName().equals(toCompare.getName()) ||
                this.getId() != toCompare.getId() ||
                this.getParentId() != toCompare.getParentId())
            return false;
        return true;
    }
}
