package src.builders;


import src.models.Category;
import src.models.Magazine;
import src.models.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CategoryBuilder {
	
	private static final String NAME_REGEX = "^[A-Z][a-z]$";
    private static final int MIN_PRODUCTS_NUMBER = 2;
    private static final int MAX_PRODUCTS_NUMBER = 20;
    private static final int MIN_ID = 1;

	public int id = 1;
	public String name = "";
	public int parent_id = 0;
	public List<Product> products = new ArrayList<>();
	
    public CategoryBuilder() { }
	
    public CategoryBuilder(String name) {
        this.name = name;
    }
    public CategoryBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CategoryBuilder setId(int id) {
        this.id = id;
        return this;
    }
    
    public CategoryBuilder setParentId(int id) {
        this.parent_id = id;
        return this;
    }
    
    public CategoryBuilder setProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    public Category build() {
        StringBuilder errorMessage = new StringBuilder("");

        Category category = new Category();
        Pattern namePattern = Pattern.compile(NAME_REGEX);

        Matcher nameMatcher = namePattern.matcher(name);

        if (!nameMatcher.matches())
            errorMessage.append("Enter correct category name");
        if (products.size() < MIN_PRODUCTS_NUMBER || products.size() > MAX_PRODUCTS_NUMBER) {
            errorMessage.append("Enter correct products list");
        }

        if (id < MIN_ID) {
            errorMessage.append("Enter correct id");
        }

        category.setName(name);
        category.setId(id);
        category.setParentId(parent_id);
        category.setProducts(products);

        return category;
    }
}
