package src.serialization;

import src.models.Category;
import src.models.Product;
import src.models.Magazine;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

public class XmlSerializator<T extends Serializable> implements Serialization<T> {
    private Class<T> type;

    public XmlSerializator(Class<T> type) {
        this.type = type;
    }

    public Class<T> getType() {
        return type;
    }

    public void setType(Class<T> type) {
        this.type = type;
    }

    @Override
    public void toFile(T object, String path) throws IOException, JAXBException {
        JAXBContext context = null;
        FileWriter outFile = null;
        Marshaller marshaller = null;

        try {
            outFile = new FileWriter(path);
            context = JAXBContext.newInstance(type);
            marshaller = context.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(object, outFile);
        } finally {
            if (outFile != null) {
                outFile.close();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T fromFile(String path) throws IOException, JAXBException {
        JAXBContext context = null;
        Unmarshaller unmarshaller = null;
        FileReader inFile = null;

        try {
            inFile = new FileReader(path);
            context = JAXBContext.newInstance(type);
            unmarshaller = context.createUnmarshaller();
            return (T) unmarshaller.unmarshal(inFile);
        } finally {
            if (inFile != null) {
                inFile.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Category category = new Category(1, "Mobile Phone", 0);
        Serialization<Category> serialization = new XmlSerializator<>(Category.class);
        serialization.toFile(category, "position.xml");
        Product product = new Product(1, "Acer Aspire V531", 2, "notebook", 1, 500);
        Magazine magazine = new Magazine(1, "Chernivtsi, Holovna 58-A", "380977123201");
        category.addProduct(product);
        magazine.addCategory(category);
        Serialization<Product> xmlSerialization = new XmlSerializator<>(Product.class);
        xmlSerialization.toFile(product, "./file1.xml");
        Serialization<Magazine> xmlSerialization2 = new XmlSerializator<>(Magazine.class);
        xmlSerialization2.toFile(magazine, "file2.xml");
    }
}
