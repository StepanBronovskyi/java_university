package src.builders;

import src.models.Category;
import src.models.Product;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ProductBuilder {

	private static final String TITLE_REGEX = "^[A-Z][a-z][0-9]$";
    private static final String DESCRIPTION_REGEX = "^[A-Z][a-z][0-9]$";
    private static final int MIN_TITLE_SIZE = 6;
    private static final int MAX_TITLE_SIZE = 36;
    private static final int MIN_DESCRIPTION_SIZE = 6;
    private static final int MAX_DESCRIPTION_SIZE = 120;
    private static final int MIN_PRODUTC_COUNT = 1;
    private static final int MAX_PRODUTC_COUNT = 999;
    private static final int MIN_PRODUCT_PRICE = 1;
    private static final int MAX_PRODUCT_PRICE = 9999;
    

    public int id;
	public String title;
	public int category_id;
	public String description;
	public int count;
	public double price;
	
	public ProductBuilder() {}
	
	
	public ProductBuilder setId(int id) {
        this.id = id;
        return this;
    }
	
	public ProductBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

	public ProductBuilder setCategory_id(int id) {
        this.category_id = id;
        return this;
    }

    public ProductBuilder setDescription(String description) {
        this.description = description;
        return this;
    }
    
    public ProductBuilder setCount(int count) {
        this.count = count;
        return this;
    }	
    
    public ProductBuilder setPrice(double price) {
        this.price = price;
        return this;
    }
    public Product build() {
        StringBuilder errorMessage = new StringBuilder("");

        Product product = new Product();
        Pattern titlePattern = Pattern.compile(TITLE_REGEX);

        Matcher titleMatcher = titlePattern.matcher(title);
        
        Pattern descriptionPattern = Pattern.compile(DESCRIPTION_REGEX);

        Matcher descriptionMatcher = descriptionPattern.matcher(description);

        if (!titleMatcher.matches())
            errorMessage.append("Enter correct product title");
        if (!descriptionMatcher.matches())
            errorMessage.append("Enter correct product description");
        if (title.length() < MIN_TITLE_SIZE || title.length() > MAX_TITLE_SIZE) {
            errorMessage.append("Enter correct products title length");
        }
        if (description.length() < MIN_DESCRIPTION_SIZE || description.length() > MAX_DESCRIPTION_SIZE) {
            errorMessage.append("Enter correct products description length");
        }
        if (count < MIN_PRODUTC_COUNT || count > MAX_PRODUTC_COUNT) {
            errorMessage.append("Enter correct product count");
        }
        if (price < MIN_PRODUCT_PRICE || price > MAX_PRODUCT_PRICE) {
            errorMessage.append("Enter correct product price");
        }

        product.setId(id);
        product.setTitle(title);
        product.setCategory_id(category_id);
        product.setDescription(description);
        product.setCount(count);
        product.setPrice(price);

        return product;
    }
}
